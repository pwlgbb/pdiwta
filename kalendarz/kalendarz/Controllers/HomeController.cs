﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace kalendarz.Controllers
{
    public class HomeController : Controller
    {
        [Route]
        public ActionResult Index()
        {
            return View();
        }

        [Route("Stare")]
        public ActionResult Stare()
        {
            return View();
        }

        [Route("Nowe")]
        public ActionResult Nowe()
        {
            return View();
        }       

        [Route("GetData")]
        public ActionResult GetData(int monthIndex)
        {
            var taskList = new TODO[]
            {
                new TODO(){Date = DateTime.Now, TaskName = "Second"},
                new TODO(){Date = DateTime.Now.AddMonths(-1), TaskName = "First"},
                new TODO(){Date = DateTime.Now.AddMonths(1), TaskName = "Third"},
                new TODO(){Date = DateTime.Now.AddMonths(2), TaskName = "Other 1st"},
                new TODO(){Date = DateTime.Now.AddMonths(2), TaskName = "Other 2nd"},
                new TODO(){Date = DateTime.Now.AddMonths(2), TaskName = "Other 3rd"}
            };

            var q = taskList.Where(p => p.Date.Month == monthIndex);
            var data = new
            {
                Field = DateTime.Now,
                OtherField = new[] { 1, 2, 3, 4 }
            };
            return Json(q, JsonRequestBehavior.AllowGet);

        }
    }

    public class TODO
    {
        public DateTime Date { get; set; }
        public string TaskName { get; set; }
    }
}