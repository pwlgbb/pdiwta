﻿var myApp = angular.module('myApp', []);
myApp.filter('range', function () {
    return function (input, total) {
        total = parseInt(total);
        for (var i = 0; i < total; i++)
            input.push(i);
        return input;
    };
});

myApp.filter('reverse', function () {
    return function (items) {
        return items.slice().reverse();
    };
});