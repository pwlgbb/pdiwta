﻿var globalParameters1 = [];
var globalParameters2 = [];
var globalParameters3 = [];

function linearInterpolation(x) {
    var y = 0;
    for (var i = globalParameters1.length - 1; i >= 0; i--)
        y += globalParameters1[i] * Math.pow(x, i);
    return y;
}

function ChebyshevInterpolation(x) {
    var y = 0;
    for (var i = globalParameters2.length - 1; i >= 0; i--)
        y += globalParameters2[i] * Math.pow(x, i);
    return y;
}

function HermiteInterpolation(x) {
    var y = 0;
    for (var i = globalParameters3.length - 1; i >= 0; i--)
        y += globalParameters3[i] * Math.pow(x, i);
    return y;
}