﻿function Controller($scope, $http) {
    $scope.Math = Math;

    $scope.xArr = [];

    $scope.yArr = [];

    $scope.minX = -100;

    $scope.maxX = 100;

    $scope.minY = -100;

    $scope.maxY = 100;

    $scope.unitsPerTick = 5;

    $scope.width = 600;

    $scope.height = 600;

    $scope.polynomial1Params = {};

    $scope.polynomial2Params = {};

    $scope.polynomial3Params = {};

    $scope.formula = "";

    $scope.visible = false;

    $scope.clear = function () {
        for (var i = $scope.numberOfValues; i < $scope.xArr.length; i++) {
            $scope.xArr[i] = null;
        }
        for (var i = $scope.numberOfValues; i < $scope.yArr.length; i++) {
            $scope.yArr[i] = null;
        }
        $scope.moreThanZero = true;
    }

    $scope.checkDuplicates = function () {
        var test = false;
        for (var i = 0; i < $scope.xArr.length; i++) {
            if ($scope.xArr[i] == $scope.xArr[i + 1]) {
                test = true;
                break;
            }
        }
        return test;
    }

    $scope.check = function () {
        if ($scope.checkDuplicates()) {
            $scope.numberOfValues = 0;
            $scope.clear();
            $scope.moreThanZero = false;
            $scope.valVisible = false;
            $scope.visible = false;
            alert("Wartości na osi X nie mogą zawierać duplikatów!");
            return true;
        }
        else
            return false;
    }

    $scope.getParams = function () {
        for (var i = 0; i < $scope.numberOfValues; i++) {
            $scope.xArr[i] = parseFloat($scope.xArr[i]);
            $scope.yArr[i] = parseFloat($scope.yArr[i]);
            if (isNaN($scope.xArr[i]) || $scope.xArr[i] == undefined || isNaN($scope.yArr[i]) || $scope.yArr[i] == undefined)
                return;
        }
        if ($scope.check()) {
            return;
        }
        var parameters = { X: $scope.xArr, Y: $scope.yArr };
        $http({
            method: 'GET',
            url: '/GetPolynomialsParametersOfLI',
            params: parameters
        }).success(function (data, status, headers, config) {
            $scope.polynomialParams1 = data.map(function (val, ind) {
                return { "index": ind, "value": val };
            }).reverse();
            $scope.formula1 = "W(x)=";
            for (var i = data.length - 1; i >= 0; i--) {
                if (i == data.length - 1)
                    $scope.formula1 += data[i] + "*x^" + i;
                else if (data[i] != 0)
                    $scope.formula1 += "+" + data[i] + "*x^" + i;
            }
        });
        $http({
            method: 'GET',
            url: '/GetPolynomialsParametersOfChebyshevPI',
            params: parameters
        }).success(function (data, status, headers, config) {
            $scope.polynomialParams2 = data.map(function (val, ind) {
                return { "index": ind, "value": val };
            }).reverse();
            $scope.formula2 = "W(x)=";
            for (var i = data.length - 1; i >= 0; i--) {
                if (i == data.length - 1)
                    $scope.formula2 += data[i] + "*x^" + i;
                else if (data[i] != 0)
                    $scope.formula2 += "+" + data[i] + "*x^" + i;
            }
        });
        $http({
            method: 'GET',
            url: '/GetPolynomialsParametersOfHermitePI',
            params: parameters
        }).success(function (data, status, headers, config) {
            $scope.polynomialParams3 = data.map(function (val, ind) {
                return { "index": ind, "value": val };
            }).reverse();
            $scope.formula3 = "W(x)=";
            for (var i = data.length - 1; i >= 0; i--) {
                if (i == data.length - 1)
                    $scope.formula3 += data[i] + "*x^" + i;
                else if (data[i] != 0)
                    $scope.formula3 += "+" + data[i] + "*x^" + i;
            }
        });
        $scope.visible = true;
    }

    $scope.getValue = function (value) {
        if (value == null)
            return;
        for (var i = 0; i < $scope.numberOfValues; i++) {
            $scope.xArr[i] = parseFloat($scope.xArr[i]);
            $scope.yArr[i] = parseFloat($scope.yArr[i]);
            if (isNaN($scope.xArr[i]) || $scope.xArr[i] == undefined || isNaN($scope.yArr[i]) || $scope.yArr[i] == undefined)
                return;
        }
        if ($scope.check()) {
            return;
        }
        var parameters = { val: value, X: $scope.xArr, Y: $scope.yArr };
        $http({
            method: 'GET',
            url: '/ComputeInterpolatedValueOfLI',
            params: parameters
        }).success(function (data, status, headers, config) {
            $scope.result1 = data;
        });
        $http({
            method: 'GET',
            url: '/ComputeInterpolatedValueOfChebyshevPI',
            params: parameters
        }).success(function (data, status, headers, config) {
            $scope.result2 = data;
        });
        $http({
            method: 'GET',
            url: '/ComputeInterpolatedValueOfHermitePI',
            params: parameters
        }).success(function (data, status, headers, config) {
            $scope.result3 = data;
        });
        $scope.valVisible = true;
    }

    $scope.drawChart = function () {
        for (var i = 0; i < $scope.numberOfValues; i++) {
            $scope.xArr[i] = parseFloat($scope.xArr[i]);
            $scope.yArr[i] = parseFloat($scope.yArr[i]);
            if (isNaN($scope.xArr[i]) || $scope.xArr[i] == undefined || isNaN($scope.yArr[i]) || $scope.yArr[i] == undefined)
                return;
        }
        if ($scope.check()) {
            return;
        }
        var parameters = { X: $scope.xArr, Y: $scope.yArr };
        $http({
            method: 'GET',
            url: '/GetPolynomialsParametersOfLI',
            params: parameters,
            async: false
        }).success(function (data, status, headers, config) {
            globalParameters1 = data;
            draw($scope.minX, $scope.maxX, $scope.minY, $scope.maxY, $scope.unitsPerTick);
            $scope.drawVisible = true;
        });
        $http({
            method: 'GET',
            url: '/GetPolynomialsParametersOfChebyshevPI',
            params: parameters,
            async: false
        }).success(function (data, status, headers, config) {
            globalParameters2 = data;
            draw($scope.minX, $scope.maxX, $scope.minY, $scope.maxY, $scope.unitsPerTick);
            $scope.drawVisible = true;
        });
        $http({
            method: 'GET',
            url: '/GetPolynomialsParametersOfHermitePI',
            params: parameters,
            async: false
        }).success(function (data, status, headers, config) {
            globalParameters3 = data;
            draw($scope.minX, $scope.maxX, $scope.minY, $scope.maxY, $scope.unitsPerTick);
            $scope.drawVisible = true;
        });
    }

    $scope.refreshChart = function () {
        if (parseFloat($scope.unitsPerTick) <= 0 || isNaN(parseFloat($scope.unitsPerTick)))
            return;
        $scope.drawVisible = true;
        draw($scope.minX, $scope.maxX, $scope.minY, $scope.maxY, $scope.unitsPerTick);
    }

    $scope.resizeChart = function () {
        var canvas = document.getElementById("canvas");
        canvas.width = $scope.width;
        canvas.height = $scope.height;
        canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
        $scope.drawVisible = true;
        if (parseFloat($scope.unitsPerTick) <= 0 || isNaN(parseFloat($scope.unitsPerTick)))
            return;
        draw($scope.minX, $scope.maxX, $scope.minY, $scope.maxY, $scope.unitsPerTick);
    }
}