﻿function draw(minXVal, maXVal, minYVal, maxYVal, unitsPerTickVal) {
    var canvas = document.getElementById("canvas");
    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
    var graph = new Graph({
        canvasId: 'canvas',
        minX: minXVal,
        minY: minYVal,
        maxX: maXVal,
        maxY: maxYVal,
        unitsPerTick: unitsPerTickVal
    });

    graph.drawEquation(linearInterpolation, "red", 2);
    graph.drawEquation(ChebyshevInterpolation, "green", 2);
    graph.drawEquation(HermiteInterpolation, "blue", 2);
}