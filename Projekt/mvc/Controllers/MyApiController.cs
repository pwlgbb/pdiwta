﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Services;

namespace mvc.Controllers
{
    public class MyApiController : ApiController
    {
        private LinearInterpolation linearInterpolation;
        private ChebyshevPolynomialInterpolation chebyshevPolynomialInterpolation;
        private HermitePolynomialInterpolation hermitePolynomialInterpolation;

        public MyApiController(LinearInterpolation linearInterpolation, ChebyshevPolynomialInterpolation chebyshevPolynomialInterpolation, HermitePolynomialInterpolation hermitePolynomialInterpolation)
        {
            this.linearInterpolation = linearInterpolation;
            this.chebyshevPolynomialInterpolation = chebyshevPolynomialInterpolation;
            this.hermitePolynomialInterpolation = hermitePolynomialInterpolation;
        }

        [Route("Hello")]
        [HttpGet]
        public string GetHello()
        {
            return "Hello";
        }

        [Route("GetPolynomialsParametersOfLI")]
        [HttpGet]
        public double[] GetPolynomialsParametersOfLI([FromUri]double[] X, [FromUri]double[] Y)
        {
            double[][] U;
            linearInterpolation.FillMatrix(out U, X, Y);
            double[] A;
            linearInterpolation.GetPolynomialsParameters(out A, U);
            return A;
        }

        [Route("GetPolynomialsParametersOfChebyshevPI")]
        [HttpGet]
        public double[] GetPolynomialsParametersOfChebyshevPI([FromUri]double[] X, [FromUri]double[] Y)
        {
            double[][] U;
            chebyshevPolynomialInterpolation.FillMatrix(out U, X, Y);
            double[] A;
            chebyshevPolynomialInterpolation.GetPolynomialsParameters(out A, U);
            return A;
        }

        [Route("GetPolynomialsParametersOfHermitePI")]
        [HttpGet]
        public double[] GetPolynomialsParametersOfHermitePI([FromUri]double[] X, [FromUri] double[] Y)
        {
            double[][] U;
            hermitePolynomialInterpolation.FillMatrix(out U, X, Y);
            double[] A;
            hermitePolynomialInterpolation.GetPolynomialsParameters(out A, U);
            return A;
        }

        [Route("ComputeInterpolatedValueOfLI")]
        [HttpGet]
        public double ComputeInterpolatedValueOfLI([FromUri]double val, [FromUri]double[] X, [FromUri]double[] Y)
        {
            double[][] U;
            linearInterpolation.FillMatrix(out U, X, Y);
            double[] A;
            linearInterpolation.GetPolynomialsParameters(out A, U);
            return linearInterpolation.ComputeInterpolatedValue(val, A);
        }

        [Route("ComputeInterpolatedValueOfChebyshevPI")]
        [HttpGet]
        public double ComputeInterpolatedValueOfChebyshevPI([FromUri]double val, [FromUri]double[] X, [FromUri]double[] Y)
        {
            double[][] U;
            chebyshevPolynomialInterpolation.FillMatrix(out U, X, Y);
            double[] A;
            chebyshevPolynomialInterpolation.GetPolynomialsParameters(out A, U);
            return chebyshevPolynomialInterpolation.ComputeInterpolatedValue(val, A);
        }

        [Route("ComputeInterpolatedValueOfHermitePI")]
        [HttpGet]
        public double ComputeInterpolatedValueOfHermitePI([FromUri]double val, [FromUri]double[] X, [FromUri]double[] Y)
        {
            double[][] U;
            hermitePolynomialInterpolation.FillMatrix(out U, X, Y);
            double[] A;
            hermitePolynomialInterpolation.GetPolynomialsParameters(out A, U);
            return hermitePolynomialInterpolation.ComputeInterpolatedValue(val, A);
        }
    }
}
