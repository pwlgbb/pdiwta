﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace mvc
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                         "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                         "~/Scripts/angular.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                         "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/bootstrap").Include(
                        "~/Content/bootstrap.css",
                        "~/Content/bootstrap-theme.css"));

            bundles.Add(new ScriptBundle("~/bundles/Controller").Include("~/Scripts/Controller.js"));

            bundles.Add(new ScriptBundle("~/bundles/draw").Include("~/Scripts/draw.js"));

            bundles.Add(new ScriptBundle("~/bundles/filters").Include("~/Scripts/filters.js"));

            bundles.Add(new ScriptBundle("~/bundles/Global").Include("~/Scripts/Global.js"));

            bundles.Add(new ScriptBundle("~/bundles/Graph").Include("~/Scripts/Graph.js"));
        }
    }
}