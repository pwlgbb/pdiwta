﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Optimization;
using System.Web.Http;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using Services;
using mvc.Controllers;

namespace mvc
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            GlobalConfiguration.Configuration.EnsureInitialized();
            RegisterContainer();
        }

        private void RegisterContainer()
        {
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterApiControllers(typeof(MvcApplication).Assembly);
            builder.RegisterType<Gauss>().AsSelf().As<IGauss>();
            builder.RegisterType<ChebyshevPolynomial>().AsSelf().As<IPolynomial>();
            builder.RegisterType<HermitePolynomial>().AsSelf().As<IPolynomial>();
            builder.RegisterType<LinearInterpolation>().AsSelf().As<IInterpolation>();
            builder.RegisterType<ChebyshevPolynomialInterpolation>().AsSelf().As<PolynomialInterpolation>().As<IInterpolation>();
            builder.RegisterType<HermitePolynomialInterpolation>().AsSelf().As<PolynomialInterpolation>().As<IInterpolation>();
            IContainer container = builder.Build();
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}
