﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class LinearInterpolation : IInterpolation
    {
        private IGauss gauss;

        public LinearInterpolation(IGauss gauss)
        {
            this.gauss = gauss;
        }

        public double ComputeInterpolatedValue(double x, double[] A)
        {
            int n = A.Length;
            double W = 0;
            for (int i = 0; i < n; i++)
            {
                W += A[i] * Math.Pow(x, i);
            }
            return W;
        }

        public void FillMatrix(out double[][] U, double[] X, double[] Y)
        {
            if (X.Length != Y.Length)
                throw new Exception("Długości wektorów X i Y są różne!");
            int n = X.Length;
            U = new double[n][];
            for (int i = 0; i < n; i++)
            {
                U[i] = new double[n + 1];
            }
            for (int i = 0; i < n; i++)
            {
                U[i][n] = Y[i];
            }
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    U[i][j] = Math.Pow(X[i], j);
                }
            }
        }

        public void GetPolynomialsParameters(out double[] A, double[][] U)
        {
            for (int i = 0; i < U.Length; i++)
            {
                if (U[i].Length != U.Length + 1)
                    throw new Exception();
            }
            int n = U.GetLength(0);
            gauss.Compute(U);
            A = new double[n];
            for (int i = 0; i < n; i++)
            {
                A[i] = U[i][n];
            }
        }
    }
}
