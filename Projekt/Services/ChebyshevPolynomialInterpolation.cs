﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class ChebyshevPolynomialInterpolation : PolynomialInterpolation
    {
        public ChebyshevPolynomialInterpolation(IGauss gauss, ChebyshevPolynomial polynomial)
            : base(gauss, polynomial)
        {
        }
    }
}
