﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class HermitePolynomial : IPolynomial
    {
        public double Compute(uint n, double x)
        {
            if (n == 0)
            {
                return 1;
            }
            else if (n == 1)
            {
                return 2 * x;
            }
            else
            {
                return 2 * x * Compute(n - 1, x) - 2 * (n - 1) * Compute(n - 2, x);
            }
        }
    }
}
