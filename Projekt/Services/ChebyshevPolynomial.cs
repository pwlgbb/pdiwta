﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class ChebyshevPolynomial : IPolynomial
    {
        public double Compute(uint n, double x)
        {
            if (n == 0)
            {
                return 1;
            }
            else if (n == 1)
            {
                return x;
            }
            else
            {
                return 2 * x * Compute(n - 1, x) - Compute(n - 2, x);
            }
        }
    }
}
