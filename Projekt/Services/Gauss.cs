﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class Gauss : IGauss
    {
        public void Compute(double[][] a)
        {
            for (int i = 0; i < a.Length; i++)
            {
                if (a[i].Length != a.Length + 1)
                    throw new Exception();
            }
            int n = a.GetLength(0);
            double max, s, p, q;
            int l;
            for (int k = 0; k < n; k++)
            {
                max = Math.Abs(a[k][k]);
                l = k;
                for (int i = k; i < n; i++)
                {
                    if (max < Math.Abs(a[i][k]))
                    {
                        max = a[i][k];
                        l = i;
                    }
                }
                for (int j = 0; j < n + 1; j++)
                {
                    s = a[k][j];
                    a[k][j] = a[l][j];
                    a[l][j] = s;
                }
                p = a[k][k];
                for (int j = k; j < n + 1; j++)
                {
                    a[k][j] = a[k][j] / p;
                }
                for (int i = 0; i < n; i++)
                {
                    if (i != k)
                    {
                        q = a[i][k];
                        for (int j = k; j < n + 1; j++)
                        {
                            a[i][j] = a[i][j] - q * a[k][j];
                        }
                    }
                }
            }
            return;
        }
    }
}
