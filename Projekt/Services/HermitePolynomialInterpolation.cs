﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class HermitePolynomialInterpolation : PolynomialInterpolation
    {
        public HermitePolynomialInterpolation(IGauss gauss, HermitePolynomial polynomial)
            : base(gauss, polynomial)
        {
        }
    }
}
