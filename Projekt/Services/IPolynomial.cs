﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services
{
    public interface IPolynomial
    {
        double Compute(uint n, double x);
    }
}
