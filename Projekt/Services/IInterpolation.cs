﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public interface IInterpolation
    {
        double ComputeInterpolatedValue(double x, double[] A);
        void FillMatrix(out double[][] U, double[] X, double[] Y);
        void GetPolynomialsParameters(out double[] A, double[][] U);
    }
}
