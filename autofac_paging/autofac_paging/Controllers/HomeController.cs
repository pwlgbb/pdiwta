﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using autofac_paging.Models;
using PagedList;

namespace autofac_paging.Controllers
{
    public class HomeController : Controller
    {
        private IRepository repository;

        public HomeController(IRepository repository)
        {
            this.repository = repository;
        }

        [Route()]
        [Route("Home/Index")]
        public ActionResult Index(int? page)
        {
            int pageSize = 5;
            int pageNumber = (page ?? 1);

            IEnumerable<Products> products = repository.SelectProducts();
            return View(products.ToPagedList(pageNumber, pageSize));
        }
    }
}