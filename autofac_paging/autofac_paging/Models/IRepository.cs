﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace autofac_paging.Models
{
    public interface IRepository
    {
        IEnumerable<Products> SelectProducts();
    }
}