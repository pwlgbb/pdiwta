﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace autofac_paging.Models
{
    public class DbRepository : IRepository
    {
        public IEnumerable<Products> SelectProducts()
        {
            using (NorthwindEntities db = new NorthwindEntities())
            {
                return db.Products.Select(n => n).ToList();
            }
        }
    }
}